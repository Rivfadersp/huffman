import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class Main {

	public static int[] ch = new int[258];
	public static String[] prf = new String[258];
	public static String prfx = "";
	
	public static void encode(Tree tree, char symbol, String current){
		if(symbol==tree.value) prfx=current;
		if(tree.left!=null) encode(tree.left, symbol, current+"0");
		if(tree.right!=null) encode(tree.right, symbol, current+"1");
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		for(int i=0; i<258; i++){
			ch[i] = 1;
		}
		
		File input = new File("E:/input.txt");
		String file = new Scanner(input).useDelimiter("\\Z").next();
		Comparator<Tree> comparator = new TreeComparator();
		String answer = ""; 
		PrintWriter writer = new PrintWriter("E:/output.txt");
		for(int i=0; i<file.length(); i++){
			ch[(int)(file.charAt(i))]++;
			PriorityQueue<Tree> queue = new PriorityQueue<Tree>(comparator);
			for(int j=0; j<256; j++){
				if(ch[j]!=0){
					Tree tree = new Tree((char)j);
					tree.weight = ch[j];
					queue.add(tree);
				}
			}
			
			if(queue.size()==1) answer = answer + "0";
			
			while(queue.size()!=1){
				Tree first = queue.poll();
				Tree second = queue.poll();
				Tree mega = first.symbiosis(second);
				mega.weight = first.weight + second.weight;
				queue.add(mega);
				//System.out.println(mega.right.value);
			}
			
			encode(queue.poll(), file.charAt(i), "");
			answer = answer + prfx;
			if(i==file.length()-1){
				if(answer.length()<8){
					while(answer.length()!=8) answer = "0" + answer;
				}
			}
			
			if(answer.length()>=8){
				writer.write((char)Integer.parseInt(answer.substring(0,8),2));
				answer = answer.substring(8,answer.length());
			}
			
			//System.out.println(file.charAt(i)+" - " + prfx);
			prfx = "";
		}
		writer.close();
		
	
			
	}

}
