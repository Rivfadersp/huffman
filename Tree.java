
public class Tree {
	public Tree left; 	
	public Tree right;
	public char value='\u0000';
	public int weight;
	public Tree(Tree x, Tree y){
		 left = x; 
		 right = y;
	}
	
	public Tree(char x){
		value = x;
	}
	
	public Tree symbiosis(Tree y){
		Tree result = new Tree(this, y);
		return result;
	}
}
