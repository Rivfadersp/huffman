import java.util.Comparator;

public class TreeComparator implements Comparator<Tree> {
	@Override
    public int compare(Tree x, Tree y)
    {
        if (x.weight < y.weight)
        {
            return -1;
        }
        if (x.weight > y.weight)
        {
            return 1;
        }
        return 0;
    }
}
